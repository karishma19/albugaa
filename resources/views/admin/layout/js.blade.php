<!-- jQuery 3 -->
<?= Html::script('backend/js/jquery.min.js',[],IS_SECURE) ?>

<?= Html::script('backend/js/bootstrap.min.js',[],IS_SECURE) ?>
<!-- Morris.js charts -->
<?= Html::script('backend/js/raphael.min.js',[],IS_SECURE) ?>

<?= Html::script('backend/js/morris.min.js',[],IS_SECURE) ?>
<!-- Sparkline -->
<?= Html::script('backend/js/jquery.sparkline.min.js',[],IS_SECURE) ?>
<!-- jvectormap -->
<?= Html::script('backend/js/jquery-jvectormap-1.2.2.min.js',[],IS_SECURE) ?>

<?= Html::script('backend/js/jquery-jvectormap-world-mill-en.js',[],IS_SECURE) ?>
<!-- jQuery Knob Chart -->
<?= Html::script('backend/js/jquery.knob.min.js',[],IS_SECURE) ?>
<!-- daterangepicker -->
<?= Html::script('backend/js/moment.min.js',[],IS_SECURE) ?>
<?= Html::script('backend/js/daterangepicker.js',[],IS_SECURE) ?>
<!-- datepicker -->
<?= Html::script('backend/js/bootstrap-datepicker.min.js',[],IS_SECURE) ?>
<!-- Bootstrap WYSIHTML5 -->
<?= Html::script('backend/js/bootstrap3-wysihtml5.all.min.js',[],IS_SECURE) ?>
<!-- Slimscroll -->
<?= Html::script('backend/js/jquery.slimscroll.min.js',[],IS_SECURE) ?>
<!-- FastClick -->
<?= Html::script('backend/js/fastclick.js',[],IS_SECURE) ?>
<!-- AdminLTE App -->
<?= Html::script('backend/js/adminlte.min.js',[],IS_SECURE) ?>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<?= Html::script('backend/js/dashboard.js',[],IS_SECURE) ?>
<!-- AdminLTE for demo purposes -->
<?= Html::script('backend/js/demo.js',[],IS_SECURE) ?>